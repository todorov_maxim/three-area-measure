import Viewer from '../viewer';
import Loader from '../loader';

export default class App {
  constructor() {
    this.viewer = new Viewer();
    this.loader = new Loader(this.viewer);
  }
}
