import Area from '../tools/Area';

const {
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  HemisphereLight,
  OrbitControls,
  Vector2,
  Raycaster
} = window.THREE;

export default class Viewer {
  constructor() {
    this.allMeshes = [];
    this.mouse = new Vector2();
    this.raycaster = new Raycaster();
    this.init();

    this.area = new Area(this);
  }

  init() {
    // CONTAINER
    this.container = document.getElementById('threeJS');
    // option without html node
    // this.container = document.createElement('div');
    // this.container.id = 'threeJS';
    // document.body.appendChild(this.container);


    // CAMERA
    this.camera = new PerspectiveCamera(
      45,
      this.container.offsetWidth / this.container.offsetHeight,
      1,
      2000,
    );
    this.camera.position.set(100, 196, 235);

    // SCENE
    this.scene = new Scene();
    window.scene = this.scene;

    this.scene.add(this.camera);

    // RENDERER
    this.renderer = new WebGLRenderer({ antialias: true, alpha: true });
    this.renderer.setPixelRatio(1);
    this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
    // this.renderer.shadowMap.enabled = true;

    // ADD ON SCENE
    this.container.appendChild(this.renderer.domElement);

    // LIGHT
    this.initLight();

    // CONTROLS
    this.initControls();

    // RESIZE EVENT
    window.addEventListener('resize', this.onWindowResize.bind(this));
    this.container.addEventListener('mouseup', this.onModelClick.bind(this));
    this.container.addEventListener('mousedown', this.mousedown.bind(this));
    this.container.addEventListener('mousemove', this.mousemove.bind(this));

    setTimeout(() => {
      this.onWindowResize();
    }, 1000);

    this.animate();
  }

  mousedown(event) {
    this.mousedownPosition = {
      x: event.offsetX,
      y: event.offsetY,
    };
  }

  mousemove(event) {
    this.mouse.x = (event.offsetX / this.container.offsetWidth) * 2 - 1;
    this.mouse.y = -(event.offsetY / this.container.offsetHeight) * 2 + 1;

    this.raycaster.setFromCamera(this.mouse, this.camera);

    this.intersects = this.raycaster.intersectObjects([
      ...this.allMeshes,
      ...(this.area.conectorStorage.children || []),
    ]);
  }

  onModelClick(event) {
    if (
      !this.mousedownPosition ||
      !this.checkCloseEnough(this.mousedownPosition.x, event.offsetX) ||
      !this.checkCloseEnough(this.mousedownPosition.y, event.offsetY)
    ) {
      return;
    }

    if (this.intersects.length === 0) return;

    if (this.tool === 'area') {
      this.area.point(this.intersects[0]);
    }
  }

  checkCloseEnough(p1, p2) {
    return Math.abs(p1 - p2) < 6;
  }

  initControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
  }

  initLight() {
    let light = new HemisphereLight(0xffffff, 0x444444);
    light.name = 'HemisphereLight';
    light.position.set(0, 300, 0);
    this.scene.add(light);
  }

  onWindowResize() {
    this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
  }

  animate() {
    requestAnimationFrame(() => {
      this.animate();
    });

    this.controls.update();
    this.renderer.render(this.scene, this.camera);
  }
}
