const THREE = window.THREE;

export default class Area {
  constructor(viewer) {
    this.viewer = viewer;
    
    this.limits = {
      TOP: Infinity,
      BOTTOM: -Infinity,
    };
    this.init();
  }

  init() {
    this.areaStorage = new THREE.Object3D();
    this.areaStorage.name = 'areaStorage';
    this.viewer.scene.add(this.areaStorage);

    this.conectorStorage = new THREE.Object3D();
    this.conectorStorage.name = 'conectorStorage';
    this.areaStorage.add(this.conectorStorage);
    
    const btn = document.createElement('div');
    btn.className = 'btn';
    btn.innerHTML = 'AREA';
    btn.addEventListener('click', () => {
        btn.classList.toggle('active');
        if(this.viewer.tool){
            this.viewer.tool = null;
            this.clearStorage();
        }else{
            this.viewer.tool = 'area';
        }
    })
    this.viewer.container.appendChild(btn);
  }

  clearStorage() {
    let children_to_remove = [];
    this.areaStorage.traverse(child => {
      if (child.isMesh || child.isLine) {
        children_to_remove.push(child);
      }
    });
    for (var i = children_to_remove.length - 1; i >= 0; i--) {
      children_to_remove[i].parent.remove(children_to_remove[i]);
      children_to_remove[i].material.dispose();
      children_to_remove[i].geometry.dispose();
    }
  }

  point(intersect) {
    if (this.conectorStorage.children[0] && intersect.object.uuid === this.conectorStorage.children[0].uuid) {
      alert('comlete');
      this.drawArea();
      return;
    }

    if (intersect.object.name) return;

    const geometry = new THREE.SphereGeometry(1, 8, 8);
    const material = new THREE.MeshBasicMaterial({ color: 0x00adbb });
    const sphere = new THREE.Mesh(geometry, material);
    sphere.name = `conector ${this.conectorStorage.children.length}`;
    sphere.position.copy(intersect.point);
    this.conectorStorage.add(sphere);

    this.drawConnect();
  }

  drawArea() {
    this.getFacesArea(this.areaStorage.children[1].geometry);
    console.log(this.areaStorage.children[1]);
    this.getObjectArea(this.areaStorage.children[1].geometry);
  }

  drawConnect() {
    if (this.conectorStorage.children.length <= 1) return;

    const geometry = new THREE.Geometry();

    this.conectorStorage.children.forEach(mesh => {
      geometry.vertices.push(mesh.position.clone());
    });

    if (this.conectorStorage.children.length > 2) {
      const polygonGeometry = geometry.clone();

      polygonGeometry.vertices.forEach((vert, index) => {
        let face = new THREE.Face3(0, index, index + 1);
        if (index > 0 && index < polygonGeometry.vertices.length - 1) {
          polygonGeometry.faces.push(face);
          polygonGeometry.computeFaceNormals();
        }
      });
      polygonGeometry.computeBoundingSphere();

      if (this.poligon) {
        this.areaStorage.remove(this.poligon);
      }

      this.poligon = new THREE.Mesh(
        polygonGeometry,
        new THREE.MeshBasicMaterial({
          transparent: true,
          side: 2,
          opacity: 0.4,
          color: '#FCC12D',
          depthTest: true,
        }),
      );
      this.poligon.name = 'poligon';

      this.areaStorage.add(this.poligon);
    }

    let material = new THREE.LineBasicMaterial({
      color: '#FCC12D',
      side: 2,
      depthTest: false,
    });

    if (this.connectionLines) {
      this.areaStorage.remove(this.connectionLines);
    }
    this.connectionLines = new THREE.Line(geometry, material);
    this.connectionLines.name = 'connectionLines';
    this.areaStorage.add(this.connectionLines);
  }

  inside(point, vs, len) {
    var inside = false;
    for (var i = 0, j = len - 1; i < len; j = i++) {
      var intersect =
        vs[i][1] > point[1] !== vs[j][1] > point[1] &&
        point[0] <
          ((vs[j][0] - vs[i][0]) * (point[1] - vs[i][1])) /
            (vs[j][1] - vs[i][1]) +
            vs[i][0];
      if (intersect) inside = !inside;
    }
    return inside;
  }

  getFacesArea(geometry) {
    let total = 0;
    let facesArray = [];
    for (let i = 0; i < geometry.faces.length; i++) {
      facesArray.push({
        a: geometry.vertices[geometry.faces[i].a].distanceTo(
          geometry.vertices[geometry.faces[i].b],
        ),
        b: geometry.vertices[geometry.faces[i].b].distanceTo(
          geometry.vertices[geometry.faces[i].c],
        ),
        c: geometry.vertices[geometry.faces[i].c].distanceTo(
          geometry.vertices[geometry.faces[i].a],
        ),
      });
    }
    facesArray.forEach(item => {
      total += this.triangleSquare(item.a, item.b, item.c);
    });
    console.log(parseFloat(total.toFixed(2)));
    return parseFloat(total.toFixed(2));
  }

  triangleSquare(a, b, c) {
    let p = (a + b + c) / 2;
    return Math.sqrt(p * (p - a) * (p - b) * (p - c));
  }

  getObjectArea(_geometry) {
    this.directGeo = new THREE.Geometry();
    this.directGeo.fromBufferGeometry(this.viewer.allMeshes[0].geometry);

    let minY = Infinity,
      vertices = _geometry.vertices.map(ch => {
        minY = Math.min(ch.y, minY);
        return [ch.x, ch.z, ch.y];
      }),
      vertInx = [],
      totalSquare = 0,
      v = this.directGeo.vertices,
      faces = this.directGeo.faces;

    let geometry = new THREE.Geometry(),
      _faces = [];
    /* let arr:any = [];
         vertices.map(item => {
         arr.push(item[2]);
         });
         let minY = Math.min.apply(null, arr);*/
    let len = vertices.length,
      vLen = v.length;

    for (let i = 0; i < vLen; i++) {
      let _v = v[i];
      if (this.inside([_v.x, _v.z], vertices, len) && _v.y >= minY - 0.5) {
        //
        /*if (_v.y < this.limits.BOTTOM) {
                    _v = _v.clone();
                    _v.y = this.limits.BOTTOM;
                    console.log(11);
                }else if (_v.y > this.limits.TOP) {
                    _v = _v.clone();
                    _v.y = this.limits.TOP;
                    console.log(22);
                }*/
        //geometry.vertices.push(_v);
        vertInx.push(i);
      }
    }

    for (let i = 0; i < faces.length; i++) {
      if (
        vertInx.indexOf(faces[i].a) > -1 ||
        vertInx.indexOf(faces[i].c) > -1 ||
        vertInx.indexOf(faces[i].b) > -1
      ) {
        //let distances = [
        //    v[faces[i].a].distanceTo(v[faces[i].b]),
        //    v[faces[i].b].distanceTo(v[faces[i].c]),
        //    v[faces[i].c].distanceTo(v[faces[i].a])
        //];

        _faces.push(faces[i].clone());
        //sum += Math.abs(this.SignedVolumeOfTriangle(v[faces[i].c], v[faces[i].b], v[faces[i].a]));
        //totalSquare += this.triangleSquare(distances[0], distances[1], distances[2]);
      }
    }

    geometry.vertices = [];
    geometry.faces = _faces;
    for (
      let i = 0, _el = ['a', 'b', 'c'], tempInd = [];
      i < _faces.length;
      i++
    ) {
      let facEE = _faces[i],
        _vert = [];

      for (let g = 0; g < _el.length; g++) {
        let _index = _el[g];
        let _v = v[facEE[_index]];
        if (typeof tempInd[facEE[_index]] == 'undefined') {
          if (_v.y < this.limits.BOTTOM) {
            _v = _v.clone();
            _v.y = this.limits.BOTTOM;
          } else if (_v.y > this.limits.TOP) {
            _v = _v.clone();
            _v.y = this.limits.TOP;
          }

          geometry.vertices.push(_v);
          tempInd[facEE[_index]] = geometry.vertices.length - 1;
        }
        _vert.push(_v);
        facEE[_index] = tempInd[facEE[_index]];
      }

      let distances = [
        _vert[0].distanceTo(_vert[1]),
        _vert[1].distanceTo(_vert[2]),
        _vert[2].distanceTo(_vert[0]),
      ];
      totalSquare += this.triangleSquare(
        distances[0],
        distances[1],
        distances[2],
      );
    }
    this.testVolume(_geometry, geometry, geometry);
  }

  testVolume(_geometry, geometryToCut, geometryToCutBtm) {
    let newParent = new THREE.Object3D();
    this.viewer.scene.add(newParent);

    let isUp =
        _geometry.faces[0].normal.x +
          _geometry.faces[0].normal.y +
          _geometry.faces[0].normal.z >=
        0,
      direction = isUp
        ? _geometry.faces[0].normal
        : _geometry.faces[0].normal.negate(); //.negate(); //_geometry.vertices[0].y >= _geometry.vertices[2].y ? _geometry.faces[0].normal.negate() : _geometry.faces[0].normal;

    let position = new THREE.Vector3(
      (_geometry.vertices[0].x +
        _geometry.vertices[1].x +
        _geometry.vertices[2].x) /
        3,
      (_geometry.vertices[0].y +
        _geometry.vertices[1].y +
        _geometry.vertices[2].y) /
        3,
      (_geometry.vertices[0].z +
        _geometry.vertices[1].z +
        _geometry.vertices[2].z) /
        3,
    );

    /**
     * Top Plane
     * @type {THREE.Plane}
     */

    let plane = new THREE.Plane(direction);
    plane.setFromNormalAndCoplanarPoint(direction, position);

    let geom = geometryToCut;
    geom = window.sliceGeometry(geom, plane);

    var material = new THREE.MeshBasicMaterial({
      color: '#0DC9A8',
      transparent: true,
      side: 2,
      opacity: 0.6,
      depthTest: true,
    });
    let mesh = (this.topGeometry = new THREE.Mesh(geom.clone(), material));
    mesh.rotation.x = -Math.PI / 2;
    mesh.name = 'cutMesh';
    newParent.add(mesh);
  }
}
