const { MTLLoader, OBJLoader, BoxHelper } = window.THREE;

export default class Loader {
  constructor(viewer, conf) {
    this.viewer = viewer;
    this.conf = conf;
    this.loadModel();
  }

  loadModel() {
    const mtlLoader = new MTLLoader();
    const loader = new OBJLoader();

    mtlLoader.setPath(
      `https://vda-test.vap365.com/api/files/public/presign?url=https://superstorm-vda-app-consulting-dev.s3.amazonaws.com/vda-drone/Op%C4%87inaKlo%C5%A1tarIvani%C4%87/1UlMlaka/20_5_2021/All/geoobj/`
    );
    mtlLoader.setCrossOrigin("anonymous");

    mtlLoader.load(
      "Op%C4%87inaKlo%C5%A1tarIvani%C4%87_1UlMlaka_20_5_2021_All_3D_HD_high.mtl",
      (materials) => {
        loader.isGPS = true;
        materials.preload();
        loader.setMaterials(materials);
        loader.setPath(
          `https://vda-test.vap365.com/api/files/public/presign?url=https://superstorm-vda-app-consulting-dev.s3.amazonaws.com/vda-drone/Op%C4%87inaKlo%C5%A1tarIvani%C4%87/1UlMlaka/20_5_2021/All/geoobj/`
        );

        loader.load(
          "Op%C4%87inaKlo%C5%A1tarIvani%C4%87_1UlMlaka_20_5_2021_All_3D_HD_high.obj",
          (object3d) => {
            let bBOxCenter = new BoxHelper(object3d);
            let maxHeight = 0;
            let center = (this.negateCenter = bBOxCenter.geometry.boundingSphere.center.negate());
            bBOxCenter.geometry.computeBoundingBox();
            object3d.traverse((mesh) => {
              if (mesh.type === "Mesh" || mesh.type === "Points") {
                mesh.geometry.computeBoundingBox();
                mesh.geometry.translate(
                  center.x,
                  center.y,
                  Math.abs(bBOxCenter.geometry.boundingBox.min.z)
                );

                for (
                  let i = 0;
                  i < mesh.geometry.attributes.position.array.length;
                  i += 3
                ) {
                  if (maxHeight < mesh.geometry.attributes.position.array[i])
                    maxHeight = mesh.geometry.attributes.position.array[i];
                }
              }
            });
            object3d._maxHeight = maxHeight;

            object3d.rotation.x = -Math.PI / 2;

            object3d.traverse((el) => {
              if (el.isMesh) {
                this.viewer.allMeshes.push(el);
              }
            });

            // var box = new window.THREE.Box3().setFromObject(object3d);
            // const geometry = new window.THREE.BoxGeometry( box.max.x - box.min.x + 0.05, box.max.y - box.min.y + 0.05, box.max.z - box.min.z + 0.05);
            // const material = new window.THREE.MeshBasicMaterial( {color: 0x009980, transparent: true, opacity: 0.5} );
            // const cube = new window.THREE.Mesh( geometry, material );
            // this.viewer.scene.add(cube);

            this.viewer.scene.add(object3d);
          },
          (xhr) => {
            console.log(((xhr.loaded / xhr.total) * 100).toFixed(2) + "%");
          },
          (err) => {
            console.log(err);
          }
        );
      }
    );
  }
}
